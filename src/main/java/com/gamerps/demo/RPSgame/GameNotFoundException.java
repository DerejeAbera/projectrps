package com.gamerps.demo.RPSgame;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "GAME_NOT_FOUND")
public class GameNotFoundException extends Exception {
    public GameNotFoundException() {
        super("GAME_NOT_FOUND");
    }
}