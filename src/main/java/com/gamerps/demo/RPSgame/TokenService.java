package com.gamerps.demo.RPSgame;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
@AllArgsConstructor
public class TokenService {

    TokenRepository tokenRepository;
    public TokenEntity createToken() {
        TokenEntity token = TokenEntity.create();
        return tokenRepository.save(token);
    }
    public Optional<TokenEntity> getById(String tokenId){return Optional.ofNullable(tokenRepository.findById(tokenId).get());}
    public TokenEntity getToken(String tokenId) throws TokenNotFoundException {
        if(tokenRepository.findAll().stream().noneMatch(t->t.getId().equals(tokenId)))
            throw new TokenNotFoundException();
        return tokenRepository.findAll().stream().filter(t->t.getId().equals(tokenId)).findAny().get();
    }

    public void saveToken(TokenEntity token) {
        tokenRepository.save(token);
    }
    public Optional<TokenEntity> getTokenById(String tokenId) {
        return Optional.ofNullable(tokenRepository.findById(tokenId).get());
    }
}
