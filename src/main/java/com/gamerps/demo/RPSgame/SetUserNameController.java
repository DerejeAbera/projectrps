package com.gamerps.demo.RPSgame;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
@AllArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders =  "*")
public class SetUserNameController {
    TokenService tokenService;
    TokenRepository tokenRepository;
    TokenController tokenController;
    @PostMapping("/name")
    public ResponseEntity<Token> setName(@RequestBody SetUserName setUserName,
                                         @RequestHeader (value="token", required = false) String tokenId) throws TokenNotFoundException {
        TokenEntity tokenEntity=tokenService.getById(tokenId).orElseThrow(TokenNotFoundException::new);

        if(tokenEntity!=null)
            tokenEntity.setName(setUserName.getName());
        tokenRepository.save(tokenEntity);
        return new ResponseEntity<>(tokenController.toToken(tokenEntity), HttpStatus.CREATED);
    }
}
