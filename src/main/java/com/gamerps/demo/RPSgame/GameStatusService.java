package com.gamerps.demo.RPSgame;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;


@Service
@AllArgsConstructor
public class GameStatusService {
    TokenRepository tokenRepository;
    GameStatusRepository gameStatusRepository;
    public  Optional<GameStatusEntity> createGame(String tokenId) throws TokenNotFoundException {
        if(tokenRepository.findAll().stream().noneMatch(t->t.getId().equals(tokenId)))
            throw new TokenNotFoundException();
        Optional<TokenEntity> owner =tokenRepository.findAll().stream().filter(t->t.getId().equals(tokenId)).findAny();
        CreateGameStatus createGame=new CreateGameStatus(owner.get().getId(), owner.get().getName(), "","", Game.OPEN,"","");
        GameStatusEntity gameStatusEntity=new GameStatusEntity(UUID.randomUUID().toString(),owner.get(),
                null,
                createGame.getGameStatus(),
                null,
                null);
        owner.get().setOwnedGame(gameStatusEntity);
        tokenRepository.save(owner.get());
        gameStatusEntity=gameStatusRepository.save(gameStatusEntity);
        return Optional.of(gameStatusEntity);
    }


    public Optional<GameStatusEntity> getGameStatusById(String gameId) {
        return Optional.ofNullable(gameStatusRepository.findById(gameId)).get();
    }

    public GameStatusEntity joinGame(TokenEntity opponentToken, GameStatusEntity gameStatusEntity) throws Exception {
        switch (gameStatusEntity.getGame()){
            case WIN: case LOSE: case DRAW : throw new GameNotFoundException();
            case ACTIVE: throw new Exception("Game is already started ");
            case OPEN:  return joinGameIfOpen(opponentToken, gameStatusEntity);
            default : throw new IllegalStateException("Unexpected value: " + gameStatusEntity.getGame());
        }
    }
    public Optional<GameStatusEntity>getGame(TokenEntity token) {
        return Optional.ofNullable(gameStatusRepository.findById(token.getOwnedGame().getId())).get();
    }
    public String getGameIdByToken(TokenEntity token) throws GameNotFoundException {
        if(token.getOwnedGame()!=null) return token.getOwnedGame().getId();
        else if(token.getJoinGame()!=null) return token.getJoinGame().getId();
        else throw new GameNotFoundException();
    }

    public Stream<GameStatusEntity> getGameStatus(String gameId) {
        return gameStatusRepository.findAll().stream().filter(g->g.getId().equals(gameId));
    }

    public Stream<GameStatusEntity> getGamesList(String gameId) {
        return gameStatusRepository.findAll().stream().filter(g->g.getId().equals(gameId)).filter(g->g.getGame().equals(Game.OPEN));
    }

    // Makemove
    public Optional<GameStatusEntity> makeMove(Move sign, TokenEntity token) {
        GameStatusEntity getMove=null;
        if(token.getOwnedGame()!=null  &&  gameStatusRepository.findAll().stream().anyMatch(g->g.getPlayer().getId().equals(token.getId())))
        {
            GameStatusEntity owner=tokenRepository.findById(token.getId()).get().getOwnedGame();
            owner.setOwnerMove(sign);
            gameStatusRepository.save(owner);
            getMove= updateGameStatus(owner);
        }
        else if(token.getJoinGame()!=null ){
            GameStatusEntity joiner=tokenRepository.findById(token.getId()).get().getJoinGame();
            joiner.setOpponentMove(sign);
            gameStatusRepository.save(joiner);
            getMove= updateGameStatus(joiner);
        }
        return Optional.of(getMove);
    }

    private GameStatusEntity joinGameIfOpen(TokenEntity opponentToken, GameStatusEntity gameStatusEntity){
        gameStatusEntity.setOpponent(opponentToken);
        gameStatusEntity.setGame(Game.ACTIVE);
        opponentToken.setJoinGame(gameStatusEntity);
        opponentToken.setName(opponentToken.getName());
        tokenRepository.save(opponentToken);
        return gameStatusRepository.save(gameStatusEntity);
    }


    private GameStatusEntity updateGameStatus(GameStatusEntity gameEntity) {
        Move ownerMove = gameEntity.getOwnerMove();
        Move opponentMove = gameEntity.getOpponentMove();
        if(ownerMove!=null && opponentMove!=null){
            updateStatus(gameEntity, ownerMove, opponentMove);
        }
        return gameEntity;
    }
    private void updateStatus(GameStatusEntity gameEntity, Move ownerMove, Move opponentMove) {
        switch (ownerMove) {
            case ROCK: {

                if(opponentMove==Move.ROCK)
                    gameEntity.setGame(Game.DRAW);
                else if(opponentMove==Move.SCISSORS)
                    gameEntity.setGame(Game.WIN);
                else
                    gameEntity.setGame(Game.LOSE);
            }
            case PAPER:{
                if(opponentMove==Move.PAPER)
                    gameEntity.setGame(Game.DRAW);
                else if(opponentMove==Move.ROCK)
                    gameEntity.setGame(Game.WIN);
                else
                    gameEntity.setGame(Game.LOSE);
            }
            case SCISSORS:{
                if(opponentMove==Move.SCISSORS)
                    gameEntity.setGame(Game.DRAW);
                else if(opponentMove==Move.PAPER)
                    gameEntity.setGame(Game.WIN);
                else
                    gameEntity.setGame(Game.LOSE);

            }
            default: gameEntity.setGame(Game.ACTIVE);
        }
        gameStatusRepository.save(gameEntity);
    }






}
