package com.gamerps.demo.RPSgame;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class Token {
    String id;
    String ownedGameId;
    String name;
    String joinedGameId;
}
