package com.gamerps.demo.RPSgame;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity(name="games")
public class GameStatusEntity {
    @Id @Column(columnDefinition = "varchar(100)") String id;

    @OneToOne(mappedBy = "ownedGame")
    TokenEntity player;

    @OneToOne(mappedBy = "joinGame")
    TokenEntity opponent;

    @Enumerated(EnumType.STRING)
    Game game;
    @Enumerated(EnumType.STRING)
    Move ownerMove;
    @Enumerated(EnumType.STRING)
    Move opponentMove;
}
