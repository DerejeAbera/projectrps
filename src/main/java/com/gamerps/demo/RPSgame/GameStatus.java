package com.gamerps.demo.RPSgame;

import lombok.Value;

@Value
public class GameStatus {
    String id;
    String ownerTokenId;
    String ownerName;
    String opponentTokenId;
    String opponentName;
    String gameStatus;
    String ownerMove;
    String opponentMove;
}