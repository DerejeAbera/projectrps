package com.gamerps.demo.RPSgame;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class SetUserName {
    String name;

    @JsonCreator
    public SetUserName(@JsonProperty("name")  String name) {
        super();
        this.name = name;
    }
}