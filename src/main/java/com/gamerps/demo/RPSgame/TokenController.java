package com.gamerps.demo.RPSgame;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
@RestController
@RequestMapping("/auth")
@AllArgsConstructor

@CrossOrigin(origins = "*", allowedHeaders = "*")
public class TokenController {
    TokenService tokenService;
    @GetMapping("/token")
    public String createToken() {
        return tokenService.createToken().getId();
    }

    @GetMapping("/{id}")
    public Token getTokenById(@PathVariable String id) throws TokenNotFoundException {
        return tokenService.getTokenById(id).map(this::toToken).orElseThrow(TokenNotFoundException::new);
    }
    public Token toToken(TokenEntity tokenEntity){
        return new Token(
                tokenEntity.getId(),
                (tokenEntity.getOwnedGame()!=null) ? tokenEntity.getOwnedGame().getId(): "",
                tokenEntity.getName(),
                (tokenEntity.getJoinGame()!=null ) ? tokenEntity.getJoinGame().getId():""
        );
    }
}
