package com.gamerps.demo.RPSgame;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.UUID;

@Data
@AllArgsConstructor()
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity(name="tokens")
public class TokenEntity {
    @Id
    @Column(columnDefinition = "varchar(100)") String id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="owned_game_id",referencedColumnName = "id")
    GameStatusEntity ownedGame;

    String name;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="join_game_id",referencedColumnName = "id")
    GameStatusEntity joinGame;

    public static TokenEntity create() {
        return new TokenEntity(UUID.randomUUID().toString(),null,"", null);
    }
}

