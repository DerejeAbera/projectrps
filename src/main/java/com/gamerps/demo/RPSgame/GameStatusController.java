package com.gamerps.demo.RPSgame;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/games")
@AllArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders =  "*")
public class GameStatusController {
    GameStatusService gameStatusService;
    TokenService tokenService;
    @GetMapping("/start")
    public GameStatus startGame(@RequestHeader(value="token", required = false) String tokenId) throws GameNotFoundException, TokenNotFoundException {
        return gameStatusService.createGame(tokenId).map(this::toGameStatus)
                .orElseThrow(GameNotFoundException::new);
    }


    @GetMapping("/join/{gameId}")
    public GameStatus joinGame(@PathVariable String gameId,
                               @RequestHeader (value="token", required = false) String tokenId) throws Exception {

        TokenEntity opponentToken = tokenService.getToken(tokenId);
        GameStatusEntity gameStatusEntity =gameStatusService.getGameStatusById(gameId).orElseThrow(GameNotFoundException::new);
        return toGameStatus(gameStatusService.joinGame(opponentToken, gameStatusEntity));

    }
    @GetMapping("/{gameId}")
    public GameStatus gameInfo(
            @PathVariable String gameId,
            @RequestHeader (value="token", required = false) String tokenId) throws GameNotFoundException, TokenNotFoundException {
        TokenEntity token = tokenService.getToken(tokenId);
        if(token.getOwnedGame()==null) throw  new GameNotFoundException();
        return gameStatusService.getGame(token).map(this::toGameStatus).orElseThrow(GameNotFoundException::new);
    }

    @GetMapping("/status")
    public GameStatus gameStatus(@RequestHeader (value="token", required = false) String tokenId) throws GameNotFoundException, TokenNotFoundException {
        TokenEntity token= tokenService.getToken(tokenId);
        String gameId=gameStatusService.getGameIdByToken(token);
        return gameStatusService.getGameStatus(gameId).map(this::toGameStatus).findAny().orElseThrow(GameNotFoundException::new);
    }

    @GetMapping("")
    public List<GameStatus> getGamesList(@RequestHeader (value="token", required = false) String tokenId) throws TokenNotFoundException, GameNotFoundException {
        TokenEntity token= tokenService.getToken(tokenId);
        String gameId=gameStatusService.getGameIdByToken(token);

        return gameStatusService.getGamesList(gameId).map(this::toGameStatus).collect(Collectors.toList());
    }

////////
    @GetMapping("/move/{sign}")
    public GameStatus makeMove(@RequestHeader(value = "token") String tokenId,
                               @PathVariable Move sign) throws TokenNotFoundException, GameNotFoundException {
        TokenEntity token = tokenService.getToken(tokenId);
        tokenService.saveToken(token);
        return gameStatusService.makeMove(sign, token).map(this::toGameStatus).orElseThrow(GameNotFoundException::new);
    }

    private GameStatus toGameStatus(GameStatusEntity gameEntity) {
        return new GameStatus(
                gameEntity.getId(),
                (gameEntity.getPlayer()!=null)? gameEntity.getPlayer().getId():"",
                (gameEntity.getPlayer()!=null)?  gameEntity.getPlayer().getName():"",
                (gameEntity.getOpponent()!=null)? gameEntity.getOpponent().getId():"",
                (gameEntity.getOpponent()!=null)? gameEntity.getOpponent().getName():"",
                gameEntity.getGame().toString(),
                (gameEntity.getOwnerMove()!=null)?  gameEntity.getOwnerMove().toString(): "",
                (gameEntity.getOpponentMove()!=null)? gameEntity.getOpponentMove().toString() :""
        );
    }
}
