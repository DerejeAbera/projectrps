package com.gamerps.demo.RPSgame;

import lombok.Value;

@Value
public class CreateGameStatus {
    String ownerTokenId;
    String ownerName;
    String opponentTokenId;
    String opponentName;
    Game gameStatus;
    String move;
    String opponentMove;
}
